package agileengine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ElementFinder {
    private static Logger LOGGER = LoggerFactory.getLogger(ElementFinder.class);

    private static final String CHARSET_NAME = "utf8";

    private List<String> attrsToCheck = Arrays.asList("class","href","title");

    private Map<String,String> requiredAttrs = new HashMap<>();

    private String requiredText;


    public void readOriginal(File htmlFile, String targetElementId) throws IOException {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            Element element = doc.getElementById(targetElementId);
            this.requiredText = element.text();

            for(String attributeName:this.attrsToCheck){
                this.requiredAttrs.put(attributeName,element.attr(attributeName));
            }
    }

    public Optional<Element> findElement(File htmlFile){
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            List<Element> matchingElements = doc.getAllElements().stream()
                    .filter(element -> {
                                boolean found = false;
                                for (Map.Entry<String,String> entry: this.requiredAttrs.entrySet()) {
                                    if(element.attr(entry.getKey()).equals(entry.getValue())){
                                        found = true;
                                        break;
                                    }
                                }
                                return found;
                            }
                    )
                    .collect(Collectors.toList());

            if(matchingElements.isEmpty()){
                LOGGER.error("No required elements in file [{}]", htmlFile.getAbsolutePath());
                return Optional.empty();
            }

            // found only one result so returning it
            if(matchingElements.size()==1){
                return Optional.of(matchingElements.get(0));
            }

            // found multiple results, will return the one with the most matching attributes
            return matchingElements.stream().collect(
                    Collectors.maxBy(Comparator.comparingInt(this::score))
            );

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    public String getRequiredElementPath(String htmlFilePath){
        Optional<Element> opFoundElement = findElement(htmlFilePath);
        if(!opFoundElement.isPresent()){
            return "";
        }
        List<String> path = new LinkedList<>();
        Element foundElement = opFoundElement.get();

        Element currentElement = foundElement;
        do{
            String pathItem;
            Tag tag = currentElement.tag();
            pathItem = tag.getName();

            if(!currentElement.siblingElements().isEmpty()){
                pathItem=pathItem+"["+currentElement.elementSiblingIndex()+"]";
            }

            path.add(0,pathItem);
            currentElement = currentElement.parent();
        }while (currentElement.hasParent());

        return String.join("->",path);
    }

    public Optional<Element> findElement(String htmlFilePath){
        return this.findElement(new File(htmlFilePath));
    }

    private int score(Element element){
        int elementMaches = 0;

        for (Map.Entry<String,String>entry: this.requiredAttrs.entrySet()){
            if(element.attr(entry.getKey()).equals(entry.getValue())){
                ++elementMaches;
            }
        }
        if(element.text().equals(this.requiredText)){
            ++elementMaches;
        }

        return  elementMaches;
    }

}
