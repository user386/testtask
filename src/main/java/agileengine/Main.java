package agileengine;

import java.io.File;

public class Main {

    public static void main(String[] args){
        if(args.length<2 || args.length>3){
            System.out.println("Launch parameters are \" 'origin file' 'file to check' <id to check>\"");
            return;
        }
        String originFilePath = args[0];
        String fileToCheck = args[1];
        String idToCheck = args.length==3 ? args[2] : "make-everything-ok-button";

        ElementFinder elementFinder = new ElementFinder();
        try {
            elementFinder.readOriginal(new File(originFilePath),idToCheck);
        }catch (Exception e){
            System.out.println(String.format("Failed to read original file [%s], exiting",originFilePath));
            return;
        }
        String requiredElementPath = elementFinder.getRequiredElementPath(fileToCheck);
        if(requiredElementPath.isEmpty()){
            System.out.println("No matched element found");
        }else {
            System.out.println(requiredElementPath);
        }
    }

}
