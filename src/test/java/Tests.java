import agileengine.ElementFinder;
import org.jsoup.nodes.Element;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Tests {

    static ElementFinder finder;

    @BeforeClass
    public static void init() throws IOException {
        finder = new ElementFinder();
        finder.readOriginal(new File("./samples/sample-0-origin.html"),"make-everything-ok-button");
    }

    @Test
    public void testOrigin(){
        Element element = finder.findElement(new File("./samples/sample-0-origin.html")).get();

        assertEquals("make-everything-ok-button" ,element.id());
        assertEquals("Make everything OK" ,element.text());
    }

    @Test
    public void testEvilTwin(){
        Element element = finder.findElement(new File("./samples/sample-1-evil-gemini.html")).get();

        assertEquals("btn btn-success" ,element.className() );
    }

    @Test
    public void testContainerAndClone(){
        Element element = finder.findElement(new File("./samples/sample-2-container-and-clone.html")).get();

        assertEquals("btn test-link-ok" ,element.className() );
    }

    @Test
    public void testTheEscape(){
        Element element = finder.findElement(new File("./samples/sample-3-the-escape.html")).get();

        assertEquals("btn btn-success" ,element.className() );
    }

    @Test
    public void testTheMash(){
        Element element = finder.findElement(new File("./samples/sample-4-the-mash.html")).get();

        assertEquals("Do all GREAT" ,element.text() );

    }
}
