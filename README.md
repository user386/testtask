This application searches elements with given id in provided html file,
then looks for similar elements in a modified file and outputs xml path for the found element 

Dependencies

This application uses JSoup library for parsing html files and SLF4J logger.
Dependencies are included in provided .jar file

Execution

Application is packaged to a jar file with dependencies.
The file is xmlfinder.jar located in folder root, a copy is in downloads.

The application is executed from command line by calling:
java -jar xmlfinder.jar "original file path" "modified file path"


Sample input

java -jar xmlfinder.jar ~/samples/sample-0-origin.html ~/samples/sample-1-evil-gemini.html 

Sample output

html->body[1]->div[0]->div[1]->div[2]->div[0]->div->div[1]->a[1]
